const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        view: path.join(__dirname, 'src', 'main', 'webapp', 'static', 'script', 'main.js'),
        index: path.join(__dirname, 'src', 'main', 'webapp', 'static', 'script', 'clocks.js'),
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: "style!css"
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ["@babel/plugin-proposal-class-properties"]
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
    ],
    resolve: {
        modules: [
            path.join(__dirname, 'src', 'main', 'webapp', 'static', 'script'),
            path.join(__dirname, 'node_modules'),
        ],
        alias: {
            vue: 'vue/dist/vue.js'
        },
    }
};