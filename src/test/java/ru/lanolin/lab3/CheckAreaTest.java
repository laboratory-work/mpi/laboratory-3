package ru.lanolin.lab3;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.Assert;
import org.junit.Test;
import ru.lanolin.lab3.dto.PointDTO;

import java.util.ArrayList;
import java.util.List;

public class CheckAreaTest extends Assert {

	@Data
	@AllArgsConstructor
	public static class PointInternal {
		String x;
		String y;
		String r;
		boolean check;
	}

	@Test
	public void test_001(){
		PointInternal p = new PointInternal("-2.727272727272727", "1.9090909090909092", "1", false);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_002(){
		PointInternal p = new PointInternal("-2.727272727272727", "1.9090909090909092", "2", false);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_003(){
		PointInternal p = new PointInternal("-2.727272727272727", "1.9090909090909092", "3", false);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_004(){
		PointInternal p = new PointInternal("-2.727272727272727", "1.9090909090909092", "4", false);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_005(){
		PointInternal p = new PointInternal("-2.727272727272727", "1.9090909090909092", "5", true);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_006(){
		PointInternal p = new PointInternal("-3.090909090909091", "1.5909090909090908", "5", true);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_007(){
		PointInternal p = new PointInternal("-3.590909090909091", "1.0909090909090908", "5", true);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_008(){
		PointInternal p = new PointInternal("-2.909090909090909", "1.0909090909090908", "5", true);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}

	@Test
	public void test_009(){
		PointInternal p = new PointInternal("-2.6363636363636362", "1.0909090909090908", "5", true);
		assertEquals(PointDTO.isHit(p.getX(), p.getY(), p.getR()), p.isCheck());
	}
}
