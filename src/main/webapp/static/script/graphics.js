
export default class Graphics {
    delta = 5;
    radius = [1, 2, 3, 4, 5];
    colors = [
        'rgba(143, 242, 255, 0.7)',
        'rgba(255, 221, 155, 0.7)',
        'rgba(186, 255, 143, 0.7)',
        'rgba(255, 143, 157, 0.7)',
        'rgba(153, 143, 255, 0.7)'
    ];
    pointColors = [
        'rgba(13, 255, 221, 1)',
        'rgba(255, 146, 1, 1)',
        'rgba(185, 255, 13, 1)',
        'rgba(255, 13, 156, 1)',
        'rgba(13, 50, 255, 1)'
    ];
    limit = { "x": {"min": -5, "max": 5}, "y": {"min": -3, "max": 5} };

    constructor(back, front){
        this.background = $(back);
        this.foreground = $(front);

        this.width = $(back).width();
        this.height = $(back).height();

        this.widthHalf = this.width / 2;
        this.heightHalf = this.height / 2;

        this.oneLineWidth = (this.widthHalf - this.delta * 3) / this.countLines;
        this.oneLineHeight = (this.heightHalf - this.delta * 3) / this.countLines;

        this.foreground.on('click', (e) => {
            let attribute = document.getElementById("sendD:submit-btn").getAttribute("disabled");
            if(attribute !== undefined && attribute === null) {
                this.sendNewPoint(e);
            }
        });

    }

    get countLines() {
        return this.radius.length;
    }

    drawBackground() {
        this.background.drawRect({
            strokeStyle: 'rgba(220, 220, 220, 1)',
            strokeWidth: 1,
            fillStyle: 'rgba(220, 220, 220, 1)',
            width: this.width,
            height: this.height,
            x:  this.widthHalf,
            y:  this.heightHalf
        }).drawRect({
            strokeStyle: 'rgba(245,245,245, 1)',
            strokeWidth: 1,
            fillStyle: 'rgba(245,245,245, 1)',
            width: (this.limit.x.max - this.limit.x.min) * this.oneLineWidth,
            height: (this.limit.y.max - this.limit.y.min) * this.oneLineHeight,
            x:  this.widthHalf - (this.limit.x.max + this.limit.x.min) * this.oneLineWidth / 2 ,
            y:  this.heightHalf - (this.limit.y.max + this.limit.y.min) * this.oneLineWidth / 2
        });

        this.background.drawLine({
            strokeStyle: '#000',
            strokeWidth: 3,
            startArrow: true,
            arrowRadius: this.delta * 2,
            arrowAngle: 60,
            x1: this.widthHalf, y1: this.delta,
            x2: this.widthHalf, y2: this.height
        }).drawLine({
            strokeStyle: '#000',
            strokeWidth: 3,
            endArrow: true,
            arrowRadius: this.delta * 2,
            arrowAngle: 60,
            x1: 0, y1: this.heightHalf,
            x2: this.width - this.delta, y2: this.heightHalf
        });

        $.each(this.radius, (i, element) => {
            this.background
                .drawLine(this.getHath(
                    this.widthHalf - this.oneLineWidth * (i + 1),
                    this.widthHalf - this.oneLineWidth * (i + 1),
                    this.heightHalf - this.delta,
                    this.heightHalf + this.delta
                ))
                .drawText(this.getText(
                    this.widthHalf - this.oneLineWidth * (i + 1) - this.delta / 2,
                    this.heightHalf + this.delta * 3,
                    -element
                ))
                .drawLine(this.getHath(
                    this.widthHalf + this.oneLineWidth * (i + 1),
                    this.widthHalf + this.oneLineWidth * (i + 1),
                    this.heightHalf - this.delta,
                    this.heightHalf + this.delta
                ))
                .drawText(this.getText(
                    this.widthHalf + this.oneLineWidth * (i + 1),
                    this.heightHalf + this.delta * 3,
                    element
                ))
                .drawLine(this.getHath(
                    this.widthHalf - this.delta,
                    this.widthHalf + this.delta,
                    this.heightHalf - this.oneLineHeight * (i + 1),
                    this.heightHalf - this.oneLineHeight * (i + 1)
                ))
                .drawText(this.getText(
                    this.widthHalf - this.delta * 2,
                    this.heightHalf - this.oneLineHeight * (i + 1),
                    element
                ))
                .drawLine(this.getHath(
                    this.widthHalf - this.delta,
                    this.widthHalf + this.delta,
                    this.heightHalf + this.oneLineHeight * (i + 1),
                    this.heightHalf + this.oneLineHeight * (i + 1)
                ))
                .drawText(this.getText(
                    this.widthHalf - this.delta * 2,
                    this.heightHalf + this.oneLineHeight  * (i + 1),
                    -element
                ));
        });
    }

    sendNewPoint(e){
        let x = e.clientX - this.foreground.offset().left - this.widthHalf + document.documentElement.scrollLeft;
        let y = -(e.clientY - this.foreground.offset().top - this.heightHalf + document.documentElement.scrollTop);

        if (x < -this.widthHalf || x > this.widthHalf) {
            return;
        }
        if (y < -this.heightHalf || y > this.heightHalf) {
            return;
        }

        x = x / this.oneLineWidth;
        y = y / this.oneLineHeight;


        document.getElementById("canvas-click:xCanvas").value = x;
        document.getElementById("canvas-click:yCanvas").value = y;

        clickCanvas();
    }

    drawArea(rs = []) {
        this.foreground.clearCanvas();

        rs.sort().reverse().forEach(value => {
            const color = this.colors[value-1];
            this.foreground
                .drawSlice({
                    strokeStyle: color,
                    strokeWidth: 1,
                    fillStyle: color,
                    radius: (this.heightHalf - this.delta * 3) / this.countLines * value,
                    x: this.widthHalf, y: this.heightHalf,
                    start: 90, end: 180,
                })
                .drawLine({
                    strokeStyle: color,
                    strokeWidth: 1,
                    fillStyle: color,
                    closed: true,
                    x1: this.widthHalf, y1: this.heightHalf,
                    x2: this.widthHalf, y2: this.heightHalf - this.oneLineHeight * value,
                    x3: this.widthHalf - this.oneLineWidth * value, y3: this.heightHalf,
                })
                .drawRect({
                    strokeStyle: color,
                    strokeWidth: 1,
                    fillStyle: color,
                    height: this.oneLineHeight * value / 2,
                    width: this.oneLineWidth * value,
                    x: this.widthHalf + this.oneLineWidth * value / 2,
                    y: this.heightHalf - this.oneLineHeight * value / 4
                });
        });
    }

    changeGraphicsRadius(radius, points) {
        this.drawArea(radius);
        this.drawPoint(points, radius);
    }

    drawPoint(points = [], selectedR = [1]) {
        points.forEach(value => {
            let x = value['x'] * this.oneLineWidth + this.widthHalf;
            let y = -value['y'] * this.oneLineHeight + this.heightHalf;

            let color = 'rgb(255,0,0)';

            if(value['hit'] === null){
                color = 'rgba(245,245,245,1)';
            }else if(value['hit'] === true){
                color = this.pointColors[value['r']-1];
            }else if(value['hit'] === false) {
                color = 'rgb(255,0,0)';
            }

            this.foreground.drawEllipse(this.getPoint(x, y, color));
        })
    }

    getPoint(x, y, color) {
        return {
            fillStyle: color,
            strokeStyle: color,
            strokeWidth: 1,
            x: x, y: y,
            width: 3, height: 3
        }
    }

    getHath(x1, x2, y1, y2) {
        return {
            strokeStyle: '#000',
            strokeWidth: 2,
            x1: x1, y1: y1,
            x2: x2, y2: y2
        };
    }

    getText(x, y, text) {
        return {
            fillStyle: '#000',
            strokeStyle: '#000',
            strokeWidth: 1,
            fontSize: 10,
            fontFamily: 'Arial',
            x: x, y: y,
            text: text
        };
    }

}