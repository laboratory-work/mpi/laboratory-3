package ru.lanolin.lab3.repo;

import lombok.Getter;
import ru.lanolin.lab3.domain.Point;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalLong;
import java.util.concurrent.atomic.AtomicLong;

public class PointRepo {
	private final EntityManager manager;

	@Getter
	private List<Point> points;

	public PointRepo() {
		this.manager = Persistence.createEntityManagerFactory( "Unit-1" ).createEntityManager();

		Query query = this.manager.createQuery(
				"select p from Point p",
				Point.class
		);

		points = query.getResultList();
		points.sort(Comparator.comparingLong(Point::getUUID));
		System.out.println(points);
	}

	public void addNewPoint(Point nPoint){
		add2DB(nPoint);
		points.add(nPoint);
	}

	private long getMaxIds(){
		OptionalLong max = points.stream().mapToLong(Point::getUUID).max();
		return max.isPresent() ? max.getAsLong() : 1;
	}

	public void addNewPoint(List<Point> points){
		AtomicLong lg = new AtomicLong(getMaxIds());
		points.forEach(point -> point.setUUID(lg.incrementAndGet()));
		add2DB(points);
		this.points.addAll(points);
	}

	private void add2DB(List<Point> points) {
		this.manager.getTransaction().begin();
		points.forEach(this.manager::persist);
		this.manager.flush();
		this.manager.getTransaction().commit();
	}

	private void add2DB(Point nPoint){
		this.manager.getTransaction().begin();
		this.manager.persist(nPoint);
		this.manager.flush();
		this.manager.getTransaction().commit();
	}

}
