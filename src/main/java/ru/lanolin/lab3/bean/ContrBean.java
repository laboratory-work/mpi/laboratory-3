package ru.lanolin.lab3.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import javax.annotation.PostConstruct;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.time.format.DateTimeFormatter;

@Getter
@NoArgsConstructor
public class ContrBean {

	private final String VERSION = "3";
	private final String VARIANT = "47777";
	private final String AUTHOR = "Киселев Артем";
	private final String GROUP = "P3214";

	private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("Y-M-d H:m:s");

	private Profiling profiling = new Profiling();

	@PostConstruct
	@SneakyThrows
	public void initPost(){
		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		ObjectName objectName = new ObjectName("ru.lanolin.lab3:type=Profiling,name=profile");
		if(!mBeanServer.isRegistered(objectName))
			mBeanServer.registerMBean(profiling, objectName);
	}

}
