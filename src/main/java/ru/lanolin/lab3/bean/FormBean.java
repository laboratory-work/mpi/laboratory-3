package ru.lanolin.lab3.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.lanolin.lab3.domain.Point;
import ru.lanolin.lab3.dto.PointDTO;
import ru.lanolin.lab3.repo.PointRepo;

import javax.annotation.PostConstruct;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class FormBean implements Serializable {

	@Setter @Getter
	private ContrBean contrBean;

	private PointDTO point;
	private PointDTO canvas;

	@Getter @Setter
	private PointRepo pointRepo;

	public FormBean() {
		this.point = new PointDTO();
		this.canvas = new PointDTO();
	}

	@PostConstruct
	public void postConstruct(){
		this.point.setR(0, true);
	}

	private void logging(String message){
		if(contrBean.getProfiling().isDebugMode())
			log.info(message);
	}

	public void changeSlider(ValueChangeEvent event){
		setX(Integer.parseInt(event.getNewValue().toString()));
		logging("Slider change from " + event.getOldValue() + " to " + event.getNewValue());
	}

	public void changeBooleanListener(ValueChangeEvent event){
		int id = Integer.parseInt(((HtmlSelectBooleanCheckbox) event.getSource()).getId().substring(1)) - 1;
		setR(id, (Boolean) event.getNewValue());
		logging("Change boolean checkbox id " + id);
	}

	public void checkPoint(){
		logging("Click to SUBMIT: " + point);
		createPointAndAddToDB(point);
	}

	public void canvasListener(){
		logging("Add to canvas " + canvas.toString());
		boolean checkR = checkSelectedRadius();
		if(checkR){
			createPointAndAddToDB(canvas);
		}
	}

	private void createPointAndAddToDB(PointDTO pointDTO) {
		List<Point> pointToDB = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			if (getR(i)) {
				Point p = new Point(0L,
						pointDTO.getX(), pointDTO.getY(), String.valueOf(i + 1),
						pointDTO.isHit(i + 1), LocalDateTime.now());
				pointToDB.add(p);
			}
		}
		pointRepo.addNewPoint(pointToDB);
	}

	public boolean checkSelectedRadius(){
		boolean[] rs = point.getR();
		return rs[0] || rs[1] || rs[2] || rs[3] || rs[4];
	}

	public boolean getReadyToSubmit(){
		boolean checkX = point.getX() != null && !point.getX().isEmpty();
		boolean checkY = point.getY() != null && !point.getY().isEmpty();
		return checkX && checkY && checkSelectedRadius();
	}

	public String getSelectedR(){
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < 5; i++){
			if(getR(i)){
				builder.append(i+1).append(",");
			}
		}
		return builder.toString();
	}

	public String getAllPoints(){
		StringBuilder builder = new StringBuilder();
		List<PointDTO> pointRepoPoints = pointRepo.getPoints()
				.stream()
				.map(point1 -> new PointDTO(point1.getX(), point1.getY(), null))
				.distinct()
				.collect(Collectors.toList());

		for (PointDTO dto : pointRepoPoints) {
			Boolean hit = false;
			int r = 0;
			for (int i = 0; i < point.getR().length; i++) {
				if(getR(i)) {
					hit = dto.isHit(i + 1);
					if (hit == null) {
						break;
					}
					if (hit) {
						r = i + 1;
						break;
					}
				}else{
					Boolean checkToNull = dto.isHit(i + 1);
					if(checkToNull == null){
						hit = null;
						break;
					}
				}
			}
			if(r == 0) r = 6;
			builder.append("{")
					.append("\"x\":").append(dto.getX()).append(",")
					.append("\"y\":").append(dto.getY()).append(",")
					.append("\"r\":").append(r).append(",")
					.append("\"hit\":").append(hit).append(",")
					.append("},");
		}

		return builder.toString();
	}

	public void setX(Integer x){
		this.point.setX(x.toString());
	}
	public Integer getX(){
		return Integer.parseInt(this.point.getX());
	}

	public void setY(String y){
		this.point.setY(y);
	}
	public String getY(){
		return this.point.getY();
	}

	public void setR(int i, boolean val){
		this.point.setR(i, val);
		this.canvas.setR(i, val);
	}
	public boolean getR(int i){
		return this.point.getR()[i];
	}

	public boolean getOneR(){ return getR(0); }
	public void setOneR(boolean oneR){ this.point.setR(0, oneR); }

	public boolean getTwoR(){ return getR(1); }
	public void setTwoR(boolean oneR){ this.point.setR(1, oneR); }

	public boolean getFreeR(){ return getR(2); }
	public void setFreeR(boolean oneR){ this.point.setR(2, oneR); }

	public boolean getFourR(){ return getR(3); }
	public void setFourR(boolean oneR){ this.point.setR(3, oneR); }

	public boolean getFiveR(){ return getR(4); }
	public void setFiveR(boolean oneR){ this.point.setR(4, oneR); }

	public void setCanvasX(String x){
		this.canvas.setX(x);
	}

	public String getCanvasX(){
		return this.canvas.getX();
	}

	public void setCanvasY(String y){
		this.canvas.setY(y);
	}

	public String getCanvasY(){
		return this.canvas.getY();
	}
}
