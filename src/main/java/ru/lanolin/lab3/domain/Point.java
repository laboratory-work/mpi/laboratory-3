package ru.lanolin.lab3.domain;

import lombok.*;
import ru.lanolin.lab3.repo.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "points")
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Point implements Serializable {

	@Id
	@Column(nullable = false)
	private Long UUID;

	@Column(nullable = false)
	private String x;

	@Column(nullable = false)
	private String y;

	@Column(nullable = false)
	private String r;

	@Column(name = "hit", nullable = true)
	private Boolean check;

	@Column(nullable = false)
	@Convert(converter = LocalDateTimeAttributeConverter.class)
	private LocalDateTime time;

}


